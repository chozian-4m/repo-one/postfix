ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

RUN set -x \
    && dnf --assumeyes upgrade \
    && dnf --assumeyes install postfix \
    && dnf clean all

COPY ./scripts/postfix_init.sh /postfix_init.sh
RUN chmod u+x /postfix_init.sh

EXPOSE 25

CMD ["/postfix_init.sh"]

HEALTHCHECK --interval=15s --timeout=10s --retries=3 CMD postfix status || exit 1
